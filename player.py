import time
import auxiliary_functions as af
import bit_board_class


class MyPlayer:
	"""Uses bit representation of the board and tries to get edges."""

	def __init__(self, my_color, opponent_color, board_size=8):
		self.name = 'novotp23'
		self.my_color = my_color
		self.opponent_color = opponent_color
		self.board_size = board_size
		self.start_time = 0
		self.max_time = 4.99
		self.clear_chart_counter = 0

		self.left_shift = self.initialize_left_shift()
		self.right_shift = self.initialize_right_shift()
		self.left_edge_delete = self.initialize_left_edge_delete()
		self.right_edge_delete = self.initialize_right_edge_delete()
		self.top_and_bot_edge = self.initialize_top_and_bottom_edge()
		self.left_and_right_edge = self.initialize_left_and_right_edge()

		self.static_eval_chart = self.init_static_eval()
		self.end_state_reached = False

	def move(self, board):
		self.start_time = time.time()
		self.clear_chart_counter += 1
		one = 1
		depth = 4
		alpha = -1000000
		beta = 1000000
		best_move_idx = -1
		timeout = False

		bit_board = bit_board_class.BitBoard(my_color=self.my_color, board_size=self.board_size, left_shift=self.left_shift, right_shift=self.right_shift, left_edge_delete=self.left_edge_delete,
			right_edge_delete=self.right_edge_delete)
		bit_board.create_bit_board(board, my_color=self.my_color, opponent_color=self.opponent_color)
		viable_moves = bit_board.viable_moves(self.my_color)

		if viable_moves is None:
			return None

		moves_ordered = af.extract_moves_from_binary(viable_moves)
		# repeat for 5 seconds
		while True:
			# go through all possible moves
			for current_move in moves_ordered:
				bit_index = current_move.move_index
				temp_board = bit_board_class.BitBoard(my_color=self.my_color, pb=bit_board.pb, ob=bit_board.ob, eb=bit_board.eb, board_size=self.board_size, left_shift=self.left_shift,
					right_shift=self.right_shift, left_edge_delete=self.left_edge_delete, right_edge_delete=self.right_edge_delete)
				temp_board.make_move(self.my_color, one << bit_index)
				move_value = self.beta_search(alpha, beta, temp_board, depth)
				if move_value is None:
					timeout = True
					break
				if move_value > alpha:
					alpha = move_value
					best_move_idx = bit_index
				current_move.move_value = move_value
			if timeout:
				break
			if self.end_state_reached:
				self.end_state_reached = False
				break
			depth += 1
			alpha = -1000000
			beta = 1000000
			moves_ordered.sort(reverse=True)
		return af.convert_bit_idx_to_coordinates(best_move_idx, self.board_size)

	def alpha_search(self, alpha, beta, bit_board: bit_board_class.BitBoard, depth):

		if time.time() - self.start_time > self.max_time:
			return None

		viable_moves = bit_board.viable_moves(self.my_color)
		depth -= 1

		# state evaluation
		if bit_board.board_is_full():
			self.end_state_reached = True
			if bin(bit_board.pb).count("1") > bin(bit_board.ob).count("1"):
				return 300 + bin(bit_board.pb).count("1") - bin(bit_board.ob).count("1")
			elif bin(bit_board.pb).count("1") < bin(bit_board.ob).count("1"):
				return -300 + bin(bit_board.pb).count("1") - bin(bit_board.ob).count("1")
			else:
				return 0
		if depth == 0:
			return af.evaluate_board(bit_board, self.static_eval_chart, self.top_and_bot_edge, self.left_and_right_edge) + (
					bin(viable_moves).count("1") - bin(bit_board.viable_moves(self.opponent_color)).count("1"))

		if viable_moves == 0:
			if bit_board.viable_moves(self.opponent_color) == 0:
				if bin(bit_board.pb).count("1") > bin(bit_board.ob).count("1"):
					return 300 + bin(bit_board.pb).count("1") - bin(bit_board.ob).count("1")
				elif bin(bit_board.pb).count("1") < bin(bit_board.ob).count("1"):
					return -300 + bin(bit_board.pb).count("1") - bin(bit_board.ob).count("1")
				else:
					return 0
			else:
				return self.beta_search(alpha, beta, bit_board, depth + 1)

		# go through all possible moves
		while viable_moves != 0:
			bit_index = af.get_first_set_bit_pos(viable_moves)
			viable_moves ^= 1 << bit_index  # delete the bit which significates current move
			temp_board = bit_board_class.BitBoard(my_color=self.my_color, pb=bit_board.pb, ob=bit_board.ob, eb=bit_board.eb, board_size=self.board_size, left_shift=self.left_shift,
				right_shift=self.right_shift, left_edge_delete=self.left_edge_delete, right_edge_delete=self.right_edge_delete)
			temp_board.make_move(self.my_color, 1 << bit_index)
			move_value = self.beta_search(alpha, beta, temp_board, depth)
			if move_value is None:  # time expired
				return None
			if move_value > alpha:
				alpha = move_value
			if alpha >= beta:
				break
		return alpha

	def beta_search(self, alpha, beta, bit_board: bit_board_class.BitBoard, depth):
		depth -= 1

		if time.time() - self.start_time > self.max_time:
			return None

		viable_moves = bit_board.viable_moves(self.opponent_color)

		# state evaluation
		if bit_board.board_is_full():
			self.end_state_reached = True
			if bin(bit_board.pb).count("1") > bin(bit_board.ob).count("1"):
				return -300 + bin(bit_board.pb).count("1") - bin(bit_board.ob).count("1")
			elif bin(bit_board.pb).count("1") < bin(bit_board.ob).count("1"):
				return 300 + bin(bit_board.pb).count("1") - bin(bit_board.ob).count("1")
			else:
				return 0
		if depth == 0:
			return af.evaluate_board(bit_board, self.static_eval_chart, self.top_and_bot_edge, self.left_and_right_edge) + (
					bin(bit_board.viable_moves(self.my_color)).count("1") - bin(viable_moves).count("1"))

		if viable_moves == 0:
			if bit_board.viable_moves(self.my_color) == 0:
				if bin(bit_board.pb).count("1") > bin(bit_board.ob).count("1"):
					return -300 + bin(bit_board.pb).count("1") - bin(bit_board.ob).count("1")
				elif bin(bit_board.pb).count("1") < bin(bit_board.ob).count("1"):
					return 300 + bin(bit_board.pb).count("1") - bin(bit_board.ob).count("1")
				else:
					return 0
			else:
				return self.alpha_search(alpha, beta, bit_board, depth + 1)
		# go through all possible moves
		while viable_moves != 0:
			bit_index = af.get_first_set_bit_pos(viable_moves)
			viable_moves ^= 1 << bit_index  # delete the bit which significates current move
			temp_board = bit_board_class.BitBoard(my_color=self.my_color, pb=bit_board.pb, ob=bit_board.ob, eb=bit_board.eb, board_size=self.board_size, left_shift=self.left_shift,
				right_shift=self.right_shift, left_edge_delete=self.left_edge_delete, right_edge_delete=self.right_edge_delete)
			temp_board.make_move(self.opponent_color, 1 << bit_index)
			move_value = self.alpha_search(alpha, beta, temp_board, depth)
			if move_value is None:
				return None
			if move_value < beta:
				beta = move_value
			if alpha >= beta:
				break
		return beta

	def get_all_valid_moves(self, board):
		"""This is obligatory function which I don't use. My get_all_valide_moves function is in bit_board_class.py"""

		bit_board = bit_board_class.BitBoard(my_color=self.my_color, board_size=self.board_size, left_shift=self.left_shift, right_shift=self.right_shift, left_edge_delete=self.left_edge_delete,
			right_edge_delete=self.right_edge_delete)
		bit_board.create_bit_board(board=board, my_color=self.my_color, opponent_color=self.opponent_color)
		viable_moves = bit_board.viable_moves(self.my_color)
		list_of_moves = []
		for bit_idx in range(self.board_size * self.board_size):
			if viable_moves >> bit_idx & 1:
				list_of_moves.append(af.convert_bit_idx_to_coordinates(bit_idx, self.board_size))
		return list_of_moves

	def initialize_left_shift(self):
		left_shift = 0
		for bit_index in range(self.board_size * self.board_size):
			if bit_index % self.board_size > 1:
				left_shift |= 1 << bit_index
		return left_shift

	def initialize_right_shift(self):
		right_shift = 0
		for bit_index in range(self.board_size * self.board_size):
			if bit_index % self.board_size < self.board_size - 2:
				right_shift |= 1 << bit_index
		return right_shift

	def initialize_right_edge_delete(self):
		left_edge_delete = 0
		for bit_index in range(self.board_size * self.board_size):
			if bit_index % self.board_size < self.board_size - 1:
				left_edge_delete |= 1 << bit_index
		return left_edge_delete

	def initialize_left_edge_delete(self):
		right_edge_delete = 0
		for bit_index in range(self.board_size * self.board_size):
			if bit_index % self.board_size > 0:
				right_edge_delete |= 1 << bit_index
		return right_edge_delete

	def init_static_eval(self):
		good_position = 50
		bad_position = -5
		static_eval = {0: good_position, self.board_size - 1: good_position, self.board_size * self.board_size - 1: good_position,
			self.board_size * self.board_size - self.board_size: good_position, self.board_size + 1: bad_position,
			self.board_size * 2 - 2: bad_position, self.board_size * (self.board_size - 1) - 2: bad_position,
			self.board_size * (self.board_size - 2) + 1: bad_position}
		return static_eval

	def initialize_top_and_bottom_edge(self):
		top_and_bottom_edge = 0
		for bit_index in range(self.board_size * self.board_size):
			if 0 == bit_index // self.board_size or bit_index // self.board_size == self.board_size - 1:
				top_and_bottom_edge |= 1 << bit_index
		return top_and_bottom_edge

	def initialize_left_and_right_edge(self):
		left_and_right_edge = 0
		for bit_index in range(self.board_size * self.board_size):
			if bit_index % self.board_size == 0 or bit_index % self.board_size == self.board_size - 1:
				left_and_right_edge |= 1 << bit_index
		return left_and_right_edge
