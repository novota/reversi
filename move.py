class Move:

	def __init__(self, move_index, move_value):
		self.move_index = move_index
		self.move_value = move_value

	def __lt__(self, other):
		return self.move_value < other.move_value

	def __str__(self):
		return str(self.move_value) + " and index is " + str(self.move_index)