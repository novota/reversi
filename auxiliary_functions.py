import move


def evaluate_board(bit_board, static_eval_chart, top_and_bot_edge, left_and_right_edge):
	return bit_board.static_evaluation(static_eval_chart) + (bit_board.stability_top_bot(top_and_bot_edge) + bit_board.stability_left_right(left_and_right_edge)) * 1.5


def evaluate_board_old(bit_board, static_eval_chart):
	return bit_board.static_evaluation(static_eval_chart)


def get_first_set_bit_pos(x):

	return (x&-x).bit_length()-1


def convert_bit_idx_to_coordinates(bit_idx, board_size):
	return bit_idx // board_size, bit_idx % board_size


def extract_moves_from_binary(binary_moves):
	result = []
	while binary_moves != 0:
		bit_index = get_first_set_bit_pos(binary_moves)
		result.append(move.Move(bit_index, 0))
		binary_moves ^= 1 << bit_index
	return result
