import time
from heapq import *
import auxiliary_functions as af
import bit_board_class



class MyPlayer:
	"""Template Docstring for MyPlayer, look at the TODOs"""

	# TODO a short description of your player

	def __init__(self, my_color, opponent_color, board_size=8):
		self.name = 'novotp23'  # TODO: fill in your username
		self.my_color = my_color
		self.opponent_color = opponent_color
		self.board_size = board_size
		self.start_time = 0
		self.max_time = 4.99
		self.counter = 0

		self.left_shift = self.initialize_left_shift()
		self.right_shift = self.initialize_right_shift()
		self.left_edge_delete = self.initialize_left_edge_delete()
		self.right_edge_delete = self.initialize_right_edge_delete()

		self.static_eval_chart = self.init_static_eval()
		self.end_state_reached = False

	def move(self, board):
		self.start_time = time.time()
		self.counter = 0
		one = 1
		depth = 4
		alpha = -1000000
		beta = 1000000
		best_move_idx = -1
		timeout = False
		moves_ordered = []

		bit_board = bit_board.BitBoard(my_color=self.my_color, board_size=self.board_size, left_shift=self.left_shift, right_shift=self.right_shift, left_edge_delete=self.left_edge_delete,
			right_edge_delete=self.right_edge_delete)
		bit_board.create_bit_board(board, my_color=self.my_color, opponent_color=self.opponent_color)
		viable_moves = bit_board.viable_moves(self.my_color)

		if viable_moves is None:
			return None

		# repeat for 5 seconds
		while True:
			# go through all possible moves
			viable_moves_temp = viable_moves
			while viable_moves_temp != 0:
				bit_index = af.get_first_set_bit_pos(viable_moves_temp)
				viable_moves_temp ^= 1 << bit_index  # delete the bit which significates current move
				temp_board = bit_board.BitBoard(my_color=self.my_color, pb=bit_board.pb, ob=bit_board.ob, eb=bit_board.eb, board_size=self.board_size, left_shift=self.left_shift,
					right_shift=self.right_shift, left_edge_delete=self.left_edge_delete, right_edge_delete=self.right_edge_delete)
				temp_board.make_move(self.my_color, one << bit_index)
				move_value = self.beta_search(alpha, beta, temp_board, depth)
				if move_value is None:
					timeout = True
					break
				if move_value > alpha:
					alpha = move_value
					best_move_idx = bit_index
				heappush(moves_ordered, (move_value, bit_index))
			if timeout:
				break
			if self.end_state_reached:
				self.end_state_reached = False
				break
			print("depth ", depth)
			depth += 1
		print("counter ", self.counter)
		return af.convert_bit_idx_to_coordinates(best_move_idx, self.board_size)

	def alpha_search(self, alpha, beta, bit_board: bit_board_class.BitBoard, depth):
		self.counter += 1
		if time.time() - self.start_time > self.max_time:
			return None

		viable_moves = bit_board.viable_moves(self.my_color)
		depth -= 1

		# state evaluation
		if bit_board.board_is_full():
			self.end_state_reached = True
			if bin(bit_board.pb).count("1") > bin(bit_board.ob).count("1"):
				return 300 + bin(bit_board.pb).count("1") - bin(bit_board.ob).count("1")
			elif bin(bit_board.pb).count("1") < bin(bit_board.ob).count("1"):
				return -300 + bin(bit_board.pb).count("1") - bin(bit_board.ob).count("1")
			else:
				return 0
		if depth == 0:
			return af.evaluate_board(bit_board, self.static_eval_chart) + bin(viable_moves).count("1")

		if viable_moves == 0:
			if bit_board.viable_moves(self.opponent_color) == 0:
				return af.who_wins(bit_board)
			else:
				return self.beta_search(alpha, beta, bit_board, depth + 1)

		# go through all possible moves
		while viable_moves != 0:
			bit_index = af.get_first_set_bit_pos(viable_moves)
			viable_moves ^= 1 << bit_index  # delete the bit which significates current move
			temp_board = bit_board.BitBoard(my_color=self.my_color, pb=bit_board.pb, ob=bit_board.ob, eb=bit_board.eb, board_size=self.board_size, left_shift=self.left_shift,
				right_shift=self.right_shift, left_edge_delete=self.left_edge_delete, right_edge_delete=self.right_edge_delete)
			temp_board.make_move(self.my_color, 1 << bit_index)
			move_value = self.beta_search(alpha, beta, temp_board, depth)
			if move_value is None:
				return None
			if move_value > alpha:
				alpha = move_value
			if alpha >= beta:
				break
		return alpha

	def beta_search(self, alpha, beta, bit_board, depth):
		self.counter += 1
		if time.time() - self.start_time > self.max_time:
			return None

		viable_moves = bit_board.viable_moves(self.opponent_color)
		depth -= 1

		# state evaluation
		if bit_board.board_is_full():
			self.end_state_reached = True
			if bin(bit_board.pb).count("1") > bin(bit_board.ob).count("1"):
				return -300 + bin(bit_board.pb).count("1") - bin(bit_board.ob).count("1")
			elif bin(bit_board.pb).count("1") < bin(bit_board.ob).count("1"):
				return 300 + bin(bit_board.pb).count("1") - bin(bit_board.ob).count("1")
			else:
				return 0
		if depth == 0:
			return af.evaluate_board(bit_board, self.static_eval_chart) - bin(viable_moves).count("1")

		if viable_moves == 0:
			if bit_board.viable_moves(self.my_color) == 0:
				return af.who_wins(bit_board)
			else:
				return self.alpha_search(alpha, beta, bit_board, depth + 1)
		# go through all possible moves
		while viable_moves != 0:
			bit_index = af.get_first_set_bit_pos(viable_moves)
			viable_moves ^= 1 << bit_index  # delete the bit which significates current move
			temp_board = bit_board.BitBoard(my_color=self.my_color, pb=bit_board.pb, ob=bit_board.ob, eb=bit_board.eb, board_size=self.board_size, left_shift=self.left_shift, right_shift=self.right_shift, left_edge_delete=self.left_edge_delete, right_edge_delete=self.right_edge_delete)
			temp_board.make_move(self.opponent_color, 1 << bit_index)
			move_value = self.alpha_search(alpha, beta, temp_board, depth)
			if move_value is None:
				return None
			if move_value < beta:
				beta = move_value
			if alpha >= beta:
				break
		return beta

	def get_all_valid_moves(self, board):
		bit_board = bit_board.BitBoard(my_color=self.my_color, board_size=self.board_size, left_shift=self.left_shift, right_shift=self.right_shift, left_edge_delete=self.left_edge_delete,
			right_edge_delete=self.right_edge_delete)
		bit_board.create_bit_board(board=board, my_color=self.my_color, opponent_color=self.opponent_color)
		viable_moves = bit_board.viable_moves(self.my_color)
		list_of_moves = []
		for bit_idx in range(self.board_size * self.board_size):
			if viable_moves >> bit_idx & 1:
				list_of_moves.append(af.convert_bit_idx_to_coordinates(bit_idx, self.board_size))
		return list_of_moves

	def initialize_left_shift(self):
		left_shift = 0
		for bit_index in range(self.board_size * self.board_size):
			if bit_index % self.board_size > 1:
				left_shift |= 1 << bit_index
		return left_shift

	def initialize_right_shift(self):
		right_shift = 0
		for bit_index in range(self.board_size * self.board_size):
			if bit_index % self.board_size < self.board_size - 2:
				right_shift |= 1 << bit_index
		return right_shift

	def initialize_right_edge_delete(self):
		left_edge_delete = 0
		for bit_index in range(self.board_size * self.board_size):
			if bit_index % self.board_size < self.board_size - 1:
				left_edge_delete |= 1 << bit_index
		return left_edge_delete

	def initialize_left_edge_delete(self):
		right_edge_delete = 0
		for bit_index in range(self.board_size * self.board_size):
			if bit_index % self.board_size > 0:
				right_edge_delete |= 1 << bit_index
		return right_edge_delete

	def init_static_eval(self):
		good_position = 10
		bad_position = -2
		static_eval = {0: good_position, self.board_size-1: good_position, self.board_size * self.board_size - 1: good_position,
						self.board_size * self.board_size - self.board_size: good_position, self.board_size + 1: bad_position,
						self.board_size * 2 - 2: bad_position, self.board_size * (self.board_size - 1) - 2: bad_position,
						self.board_size * (self.board_size - 2) + 1: bad_position}
		return static_eval
