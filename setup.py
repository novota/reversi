from setuptools import setup
from Cython.Build import cythonize

setup(
	ext_modules = cythonize("bit_board_c.pyx")
)