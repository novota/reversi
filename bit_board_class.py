class BitBoard:
	"""Bit implementation of the board."""

	def __init__(self, my_color, pb=0, ob=0, eb=0, board_size=8, left_shift=0, right_shift=0, left_edge_delete=0, right_edge_delete=0):
		self.pb = pb  # players board
		self.ob = ob  # opponents board
		self.eb = eb  # emtpy positions
		self.my_color = my_color
		self.board_size = board_size

		self.left_shift = left_shift
		self.right_shift = right_shift
		self.left_edge_delete = left_edge_delete
		self.right_edge_delete = right_edge_delete

	def create_bit_board(self, board, my_color, opponent_color):
		# TODO: try converting board to numpy board first if it is faster
		one = 1
		for bit_index in range(self.board_size * self.board_size):
			row = bit_index // self.board_size
			col = bit_index % self.board_size
			try:
				if board[row][col] == my_color:
					self.pb |= one << bit_index
				elif board[row][col] == opponent_color:
					self.ob |= one << bit_index
				else:
					self.eb |= one << bit_index
			except:
				print("out of bounds")

	def static_evaluation(self, static_evaluation_chart: dict):
		eval_me = 0
		eval_opponent = 0
		for key, value in static_evaluation_chart.items():
			if self.pb >> key & 1:
				eval_me += value
			elif self.ob >> key & 1:
				eval_opponent -= value
		return eval_me + eval_opponent

	def board_is_full(self):
		return self.eb == 0

	def stability_top_bot(self, top_and_bot_edge):
		p_top_bot = self.pb & top_and_bot_edge
		o_top_bot = self.ob & top_and_bot_edge
		my_stones = bin(p_top_bot).count("1")

		if my_stones == 0:
			return 0

		stones_turned = self.move_left(self.right_search(o_top_bot, p_top_bot), o_top_bot, p_top_bot)
		stones_turned |= self.move_right(self.left_search(o_top_bot, p_top_bot), o_top_bot, p_top_bot)

		return my_stones - bin(stones_turned).count("1")

	def stability_left_right(self, left_and_right_edge):
		p_left_right = self.pb & left_and_right_edge
		o_left_right = self.ob & left_and_right_edge
		my_stones = bin(p_left_right).count("1")

		if my_stones == 0:
			return 0

		stones_turned = self.move_top(self.bottom_search(o_left_right, p_left_right), o_left_right, p_left_right)
		stones_turned |= self.move_down(self.top_search(o_left_right, p_left_right), o_left_right, p_left_right)

		return my_stones - bin(stones_turned).count("1")

	def make_move(self, current_player, move):
		if current_player == self.my_color:
			result = self.move_left(move, self.pb, self.ob) | self.move_top_left(move, self.pb, self.ob) | self.move_top(move, self.pb, self.ob) | self.move_top_right(move, self.pb,
				self.ob) | self.move_right(move, self.pb, self.ob) | self.move_right_down(move, self.pb, self.ob) | self.move_down(move, self.pb, self.ob) | self.move_left_down(move, self.pb, self.ob)
			self.pb |= move | result
			self.ob ^= result
			self.eb ^= move
		else:
			result = self.move_left(move, self.ob, self.pb) | self.move_top_left(move, self.ob, self.pb) | self.move_top(move, self.ob, self.pb) | self.move_top_right(move, self.ob,
				self.pb) | self.move_right(move, self.ob, self.pb) | self.move_right_down(move, self.ob, self.pb) | self.move_down(move, self.ob, self.pb) | self.move_left_down(move, self.ob, self.pb)
			self.pb ^= result
			self.ob |= move | result
			self.eb ^= move

	def viable_moves(self, current_player):
		if current_player == self.my_color:
			return self.top_left_search(self.pb, self.ob) | self.top_search(self.pb, self.ob) | self.top_right_search(self.pb, self.ob) | self.right_search(self.pb,
				self.ob) | self.bottom_right_search(self.pb, self.ob) | self.bottom_search(self.pb, self.ob) | self.bottom_left_search(self.pb, self.ob) | self.left_search(self.pb, self.ob)
		else:
			return self.top_left_search(self.ob, self.pb) | self.top_search(self.ob, self.pb) | self.top_right_search(self.ob, self.pb) | self.right_search(self.ob,
				self.pb) | self.bottom_right_search(self.ob, self.pb) | self.bottom_search(self.ob, self.pb) | self.bottom_left_search(self.ob, self.pb) | self.left_search(self.ob, self.pb)

	def top_search(self, pb, ob):
		candidates = pb >> self.board_size & ob
		result = 0
		while candidates != 0:
			result |= candidates >> self.board_size & self.eb
			candidates = candidates >> self.board_size & ob
		return result

	def bottom_search(self, pb, ob):
		candidates = pb << self.board_size & ob
		result = 0
		while candidates != 0:
			result |= candidates << self.board_size & self.eb
			candidates = candidates << self.board_size & ob
		return result

	def left_search(self, pb, ob):
		pb &= self.left_shift
		ob &= self.right_edge_delete
		candidates = pb >> 1 & ob
		eb_temp = self.eb & self.right_edge_delete
		result = 0
		while candidates != 0:
			result |= candidates >> 1 & eb_temp
			candidates = candidates >> 1 & ob
		return result

	def right_search(self, pb, ob):
		pb &= self.right_shift
		ob &= self.left_edge_delete
		candidates = pb << 1 & ob
		eb_temp = self.eb & self.left_edge_delete
		result = 0
		while candidates != 0:
			result |= candidates << 1 & eb_temp
			candidates = candidates << 1 & ob
		return result

	def top_left_search(self, pb, ob):
		pb &= self.left_shift
		ob &= self.right_edge_delete
		eb_temp = self.eb & self.right_edge_delete
		candidates = pb >> self.board_size + 1 & ob
		result = 0
		while candidates != 0:
			result |= candidates >> self.board_size + 1 & eb_temp
			candidates = candidates >> self.board_size + 1 & ob
		return result

	def top_right_search(self, pb, ob):
		pb &= self.right_shift
		ob &= self.left_edge_delete
		eb_temp = self.eb & self.left_edge_delete
		candidates = pb >> self.board_size - 1 & ob
		result = 0
		while candidates != 0:
			result |= candidates >> self.board_size - 1 & eb_temp
			candidates = candidates >> self.board_size - 1 & ob
		return result

	def bottom_left_search(self, pb, ob):
		pb &= self.left_shift
		ob &= self.right_edge_delete
		eb_temp = self.eb & self.right_edge_delete
		candidates = pb << self.board_size - 1 & ob
		result = 0
		while candidates != 0:
			result |= candidates << self.board_size - 1 & eb_temp
			candidates = candidates << self.board_size - 1 & ob
		return result

	def bottom_right_search(self, pb, ob):
		pb &= self.right_shift
		ob &= self.left_edge_delete
		eb_temp = self.eb & self.left_edge_delete
		candidates = pb << self.board_size + 1 & ob
		result = 0
		while candidates != 0:
			result |= candidates << self.board_size + 1 & eb_temp
			candidates = candidates << self.board_size + 1 & ob
		return result

	def move_top_left(self, move, pb, ob):
		pb &= self.right_edge_delete
		ob &= self.right_edge_delete
		candidate = move >> self.board_size + 1 & ob
		result = 0
		while candidate != 0:
			result |= candidate
			if candidate >> self.board_size + 1 & pb != 0:
				return result
			else:
				candidate = candidate >> self.board_size + 1 & ob
		return 0

	def move_top_right(self, move, pb, ob):
		pb &= self.left_edge_delete
		ob &= self.left_edge_delete
		candidate = move >> self.board_size - 1 & ob
		result = 0
		while candidate != 0:
			result |= candidate
			if candidate >> self.board_size - 1 & pb != 0:
				return result
			else:
				candidate = candidate >> self.board_size - 1 & ob
		return 0

	def move_left(self, move, pb, ob):
		pb &= self.right_edge_delete
		ob &= self.right_edge_delete
		candidate = move >> 1 & ob
		result = 0
		while candidate != 0:
			result |= candidate
			if candidate >> 1 & pb != 0:
				return result
			else:
				candidate = candidate >> 1 & ob
		return 0

	def move_right(self, move, pb, ob):
		pb &= self.left_edge_delete
		ob &= self.left_edge_delete
		candidate = move << 1 & ob
		result = 0
		while candidate != 0:
			result |= candidate
			if candidate << 1 & pb != 0:
				return result
			else:
				candidate = candidate << 1 & ob
		return 0

	def move_right_down(self, move, pb, ob):
		pb &= self.left_edge_delete
		ob &= self.left_edge_delete
		candidate = move << self.board_size + 1 & ob
		result = 0
		while candidate != 0:
			result |= candidate
			if candidate << self.board_size + 1 & pb != 0:
				return result
			else:
				candidate = candidate << self.board_size + 1 & ob
		return 0

	def move_left_down(self, move, pb, ob):
		pb &= self.right_edge_delete
		ob &= self.right_edge_delete
		candidate = move << self.board_size - 1 & ob
		result = 0
		while candidate != 0:
			result |= candidate
			if candidate << self.board_size - 1 & pb != 0:
				return result
			else:
				candidate = candidate << self.board_size - 1 & ob
		return 0

	def move_top(self, move, pb, ob):
		candidate = move >> self.board_size & ob
		result = 0
		while candidate != 0:
			result |= candidate
			if candidate >> self.board_size & pb != 0:
				return result
			else:
				candidate = candidate >> self.board_size & ob
		return 0

	def move_down(self, move, pb, ob):
		candidate = move << self.board_size & ob
		result = 0
		while candidate != 0:
			result |= candidate
			if candidate << self.board_size & pb != 0:
				return result
			else:
				candidate = candidate << self.board_size & ob
		return 0
